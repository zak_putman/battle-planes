# README #

### What is this repository for? ###

* Demo for Battle Planes Game

### How do I get set up? ###

* Pull repository and un-zip the Windows folder. This contains a windows Application, NOTE: This will only run on windows.
* Double click Battle Planes.exe and enjoy
* Windows Control Mapping:
* 	Right click - A button (for items and stores)
* 	Movement - WASD
* 	Camera - Mouse
* 	Basic Attack - Left Click
* 	Special 1 - Right click
* 	Special 2 (Xbutton) - 1 button
* 	Special 3 (Ybutton) - 2 button
* 	Pause/Start - P
* 	Navigate pause - [ and ]
* 	Exit pause - p


### Who do I talk to? ###

* Repo owner: Zak Putman
* Email: Zak.putman@Gmail.com